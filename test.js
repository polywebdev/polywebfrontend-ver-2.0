new Vue({
  el: '#app',
  name: 'app',
  data() {
    return {
      options: {
        menu: '#menu',
        anchors: ['main', 'page2', 'page3'],
        sectionsColor: ['#123211', '#080', '#0798ec']
      },
    }
  }
});