document.body.onload = function() {
	setTimeout(function() {
		var preloader = document.getElementById('preloader');
		if (!preloader.classList.contains('done')) {
			preloader.classList.add('done');
		}
	}, 1000);
	var elem = document.querySelector('.main');
	var lastHeight = elem.clientHeight;
	var diff = lastHeight + window.screen.availHeight - window.innerHeight;
	elem.style.height = "'" + diff + "px'";
	console.log('diff=' + diff);
};

// window.addEventListener("load", function () {
// 	setTimeout(function () {
// 		window.scrollTo(0, 1);
// 	}, 0);
// })
let vh = window.innerHeight;

document.documentElement.style.setProperty('--vh', '${vh}px')

var app = new Vue({
  el: '#app',
  name: 'app',
  data: {

  },
  mounted:  function() {
		  var elem = document.querySelector('.main');
		  var diff = window.screen.availHeight - window.innerHeight;
		elem.style.paddingTop = "'"+diff+"px'";
		console.log('diff='+diff);
  },
  methods: {
	  openPopUp(src) {
		this.popUp = !this.popUp;
		console.log('Открыт ли попап: '+this.popUp);
		console.log('Source: ' + this.popUpSrc);
		this.popUpSrc = src;
		this.options.autoScrolling = !this.options.autoScrolling;
		if (!this.options.autoScrolling) {
			document.style.overflow = 'hidden';
		} 
		// if (!this.options.autoScrolling) {
		// 	document.querySelector('#html').style.overflow = 'hidden';
		// }
	  },
	//   closeModalWithEsc(e) {
	//   	if (e.keyCode === 27 && this.popUp) {
	//   		this.popUp = false
	//   	}
	//   },
	  menu() {
		  	this.menuOpen = !this.menuOpen;
			  console.log('Открыто ли меню: '+this.menuOpen);
		  this.mAnim = !this.mAnim;
	  },
	  afterLoad() {

	  },
	  service_1() {
	  	this.service1 = true;
	  	this.service2 = false;
	  	this.service3 = false;
	  },
	  service_2() {
	  	this.service1 = false;
	  	this.service2 = true;
	  	this.service3 = false;
	  },
	  service_3() {
	  	this.service1 = false;
	  	this.service2 = false;
	  	this.service3 = true;
	  },
	  zakaz_open() {
			this.zakaz_1 = !this.zakaz_1;
			console.log('Попап открыт: '+this.zakaz_1);
		},
		portfolioScroll() {
			var elem = document.querySelector('.teams') || document.getElementsByClassName('teams')[0];
			var elemForward = document.querySelector('.m_portfolio_btn') || document.getElementsByClassName('m_portfolio_btn')[0];
			var elemBack = document.querySelector('.m_portfolio_btn_back') || document.getElementsByClassName('m_portfolio_btn_back')[0];
			if (elem.scrollLeft == 0) {
				elemBack.style.display = "none";
			} else {
				elemBack.style.display = "block";
			}
			var diff = elem.scrollWidth - elem.clientWidth;
			if (Math.ceil(elem.scrollLeft) == diff) {
				elemForward.style.display = "none";
			} else {
				elemForward.style.display = "block";
			}
			
		},
	  toScrollPortfolio() {
		  var elem = document.querySelector('.teams') || document.getElementsByClassName('teams')[0];
		  var elemw = elem.clientWidth;
		  elem.scrollBy({
			  left: elemw,
			  behavior: 'smooth'
		  });
		  
		  console.log('Нажато'+elemw);
		  console.log('Прокрутка слева: ' + elem.scrollLeft);
	  },
	  toScrollPortfolioBack() {
		  var elem = document.querySelector('.teams') || document.getElementsByClassName('teams')[0];
		  var elemw = elem.clientWidth;
		  elem.scrollBy({
			  left: -elemw,
			  behavior: 'smooth'
		  });
	  },
		scroll() {
			var elem = document.querySelector('.portfolio-container') || document.getElementsByClassName('portfolio-container')[0];
			var elemBack = document.querySelector('.portfolio-btn-back') || document.getElementsByClassName('portfolio-btn-back')[0];
			var elemForward = document.querySelector('.portfolio-btn') || document.getElementsByClassName('portfolio-btn')[0];
			if (elem.scrollLeft == 0) {
				elemBack.style.display = "none";
			} else {
				elemBack.style.display = "block";
			}
			var diff = elem.scrollWidth - elem.clientWidth;
			if (Math.ceil(elem.scrollLeft) == diff) {
				elemForward.style.display = "none";
			} else {
				elemForward.style.display = "block";
			}
			// console.log(diff);
			// console.log('Прокрутка слева: '+elem.scrollLeft);
		},
		to_scroll() {
			var elem = document.querySelector('.portfolio-container') || document.getElementsByClassName('portfolio-container')[0];
			// console.log(elem.scrollLeft);			// elem.scrollLeft+=320;
			if (document.body.clientWidth < 500) {
				var length = elem.clientWidth;
			} else {
				var length = 500;
			}
			elem.scrollBy({
				left: length,
				behavior: 'smooth'
			});
		},
		to_back_scroll() {
			var elem = document.querySelector('.portfolio-container') || document.getElementsByClassName('portfolio-container')[0];
			console.log(elem.scrollLeft); // elem.scrollLeft+=320;
			if (document.body.clientWidth < 500) {
				var length = elem.clientWidth;
			} else {
				var length = 500;
			}
			elem.scrollBy({
				left: -length,
				behavior: 'smooth'
			});
		}
	},
	data() {
		return {
			options: {
				afterLoad: this.afterLoad,
				scrollBar: false,
				menu: '#menu',
				anchors: ['main_sec', 'about', 'team', 'portfolio', 'services', 'contacts'],
				sectionsColor: ['transparent', '#16161B', 'transparent'],
				navigation: false,
				navigationPosition: 'right',
				navigationTooltips: ['firstSlide', 'secondSlide'],
				autoScrolling: true,
				touchSensitivity: 15
			},
		  	menuOpen: false,
		  	service1: true,
		  	service2: false,
		  	service3: false,
		  	zakaz_1: false,
			mAnim: false,
			popUp: false,
			popUpSrc: '1'	  
		}
	}
});



// onscroll = function () {
// 	var scrolled = window.pageYOffset || document.documentElement.scrollTop;
// 	document.getElementById('showScroll').innerHTML = scrolled + 'px';
// }


