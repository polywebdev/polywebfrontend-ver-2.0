$('.right-menu-img').on("click", function() {
	$(".right-menu").css('display', 'block');
	$(".right-menu-img").css('display', 'none');
	$("html,body").css("overflow","hidden");
	$(".opacity").css("opacity", "0.5");
	$(".opacity").css("pointer-events", "none");
});

$('.menu-close').on("click", function() {
	$(".right-menu").css('display', 'none');
	$(".right-menu-img").css('display', 'block');
	$("html,body").css("overflow","hidden");
	$(".opacity").css("opacity", "1");
	$(".opacity").css("pointer-events", "auto");
});

$(document).ready(function(){
      $(".mainpages").onepage_scroll({
        sectionContainer: "section",
        responsiveFallback: 600,
        direction: "horizontal",
        loop: false,
        keyboard: true,
        pagination: false
      });
});

$('.service-1-btn').on("click", function() {
	$(".service-1-btn").addClass("btn_active");
	$(".service-2-btn").removeClass("btn_active");
	$(".service-3-btn").removeClass("btn_active");
	$(".service-1").addClass("active");
	$(".service-2").removeClass("active");
	$(".service-3").removeClass("active");
	$(".service-1-btn").css("background-image", "url('img/text_w.svg')");
	$(".service-2-btn").css("background-image", "url('img/internet.svg')");
	$(".service-3-btn").css("background-image", "url('img/design.svg')");
});

$('.service-2-btn').on("click", function() {
	$(".service-2-btn").addClass("btn_active");
	$(".service-1-btn").removeClass("btn_active");
	$(".service-3-btn").removeClass("btn_active");
	$(".service-2").addClass("active");
	$(".service-1").removeClass("active");
	$(".service-3").removeClass("active");
	$(".service-1-btn").css("background-image", "url('img/text.svg')");
	$(".service-2-btn").css("background-image", "url('img/internet_w.svg')");
	$(".service-3-btn").css("background-image", "url('img/design.svg')");
});

$('.service-3-btn').on("click", function() {
	$(".service-3-btn").addClass("btn_active");
	$(".service-2-btn").removeClass("btn_active");
	$(".service-1-btn").removeClass("btn_active");
	$(".service-3").addClass("active");
	$(".service-2").removeClass("active");
	$(".service-1").removeClass("active");
	$(".service-1-btn").css("background-image", "url('img/text.svg')");
	$(".service-2-btn").css("background-image", "url('img/internet.svg')");
	$(".service-3-btn").css("background-image", "url('img/design_w.svg')");
});

$('.submit-btn').on("click", function() {
	$(".zakaz").css("display", "block");
	$(".opacity").css("opacity", "0.2");
	$(".opacity").css("pointer-events", "none");
	$(".zakaz").css("position", "fixed");
	$("html,body").css("overflow","hidden");
});

$('.fa-times').on("click", function() {
	$(".zakaz").css("display", "none");
	$(".opacity").css("opacity", "1");
	$(".opacity").css("pointer-events", "auto");
	$("html,body").css("overflow","hidden");
});

document.body.onload = function() {
	setTimeout(function() {
		var preloader = document.getElementById('preloader');
		if (!preloader.classList.contains('done')) {
			preloader.classList.add('done');
		}
	}, 1000);
}

$('.btn-next-right').click(function() {
	var currentPage = $('.about_min_content.curry');
	var currentPageIndex = $('.about_min_content.curry').index();
	var nextPageIndex = currentPageIndex + 1;
	var nextPage = $('.about_min_content').eq(nextPageIndex);
	currentPage.fadeOut(1000);
	currentPage.removeClass('curry');

	if (nextPageIndex == ($('.about_min_content:last').index() + 1 )) {
		$('.about_min_content').eq(0).fadeIn(1000);
		$('.about_min_content').eq(0).addClass('curry');
	} else {
		nextPage.fadeIn(1000);
		nextPage.addClass('curry');
	}
});

$('.fa-chevron-left').click(function() {
	var currentPage = $('.main_works.curry');
	var currentPageIndex = $('.main_works.curry').index();
	var prevPageIndex = currentPageIndex - 1;
	var prevPage = $('.main_works').eq(prevPageIndex);

	currentPage.fadeOut(1000);
	currentPage.removeClass('curry');
	prevPage.fadeIn(1000);
	prevPage.addClass('curry');
});

setInterval(function() {
	var currentPage = $('.main_works.curry');
	var currentPageIndex = $('.main_works.curry').index();
	var nextPageIndex = currentPageIndex + 1;
	var nextPage = $('.main_works').eq(nextPageIndex);
	currentPage.fadeOut(1000);
	currentPage.removeClass('curry');

	if (nextPageIndex == ($('.main_works:last').index() + 1 )) {
		$('.main_works').eq(0).fadeIn(1000);
		$('.main_works').eq(0).addClass('curry');
	} else {
		nextPage.fadeIn(1000);
		nextPage.addClass('curry');
	}
}, 5000);










